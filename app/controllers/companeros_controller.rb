class CompanerosController < ApplicationController
  before_action :set_companero, only: [:show, :edit, :update, :destroy]



  # GET /companeros
  # GET /companeros.json
  def index
    @companeros = Companero.order(:dueno)
    if params[:search]
      @companeros = Companero.search(params[:search]).order(:dueno)
    else
      @companeros = Companero.all.order(:dueno)
    end
  end

  class CompaneroPdf < Prawn::Document
    def initialize(companero)
        super(top_margin: 70, left_margin: 50, right_margin: 50)
        @companero = companero
        companero_id
        
    end

    def companero_id
      pdf = Prawn::Document.new
      text "Mascota #{@companero.nombre}", size:30, style: :bold
      text ("")
      text "____________________________________________________________________________", size: 12
      text "\n"
      text "Edad: ", size: 14, style: :bold
      text "#{@companero.edad}", size: 12
      text "\n"
      text "Raza: ", size: 14, style: :bold
      text "#{@companero.raza}", size: 12
      text "\n"
      text "Descripción: ", size: 14, style: :bold
      text "#{@companero.descripcion}", size: 12
      text "\n"
      text "Color: ", size: 14, style: :bold
      text "#{@companero.color}", size: 12
      text "\n"
      text "Peso: ", size: 14, style: :bold
      text "#{@companero.peso}", size: 12
      text "\n"
      text "Tamaño: ", size: 14, style: :bold
      text "#{@companero.tamano}", size: 12
      text "\n"
      text "Animal: ", size: 14, style: :bold
      text "#{@companero.animal}", size: 12
      text "\n"
      text "Dueño: ", size: 14, style: :bold
      text "#{@companero.dueno}", size: 12
      text "\n"
      text "Dirrección: ", size: 14, style: :bold
      text "#{@companero.direccion}", size: 12
      text "\n"
      text "Telefono: ", size: 14, style: :bold
      text "#{@companero.contacto}", size: 12
      end    
end
  # GET /companeros/1
  # GET /companeros/1.json
  def show
    @companero = Companero.find(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        pdf = CompaneroPdf.new(@companero)
        send_data pdf.render, filename: "companero_#{@companero.nombre}.pdf",
                              type: "application/pdf",
                              disposition: "inline"
        end
      end
  end

  # GET /companeros/new
  def new
    @companero = Companero.new
  end

  # GET /companeros/1/edit
  def edit
  end

  # POST /companeros
  # POST /companeros.json
  def create
    @companero = Companero.new(companero_params)

    respond_to do |format|
      if @companero.save
        format.html { redirect_to @companero, notice: 'La mascota fue creada con exito.' }
        format.json { render :show, status: :created, location: @companero }
      else
        format.html { render :new }
        format.json { render json: @companero.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companeros/1
  # PATCH/PUT /companeros/1.json
  def update
    respond_to do |format|
      if @companero.update(companero_params)
        format.html { redirect_to @companero, notice: 'La mascota fue editada con exito.' }
        format.json { render :show, status: :ok, location: @companero }
      else
        format.html { render :edit }
        format.json { render json: @companero.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companeros/1
  # DELETE /companeros/1.json
  def destroy
    @companero.destroy
    respond_to do |format|
      format.html { redirect_to companeros_url, notice: 'La mascota fue eliminada con exito.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_companero
      @companero = Companero.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def companero_params
      params.require(:companero).permit(:fecha, :nombre, :edad, :raza, :dueno, :descripcion, :color, :peso, :tamano, :foto, :animal, :direccion, :contacto, :email)
    end
end
