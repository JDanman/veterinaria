class VacunasController < ApplicationController
  def index
    @vacunas = Vacuna.all
  end
  
########################################################################
  def create
    @companero = Companero.find(params[:companero_id])
    @vacuna = @companero.vacunas.create(vacuna_params)
    redirect_to companero_path(@companero)
  end
 
  private
    def vacuna_params
      params.require(:vacuna).permit(:nombre, :doctor, :aplicacion, :tipo, :fecha)
    end
################################################
end