class Vacuna < ApplicationRecord
    belongs_to :companero

    validates :nombre, :aplicacion, :doctor, :tipo, :fecha, presence: true
end
