class Companero < ApplicationRecord
    has_many :vacunas
    
    mount_uploader :foto, FotoUploader
    validates :contacto, length: {maximum: 10}
    validates :edad,  numericality: {only_integer: true, message: "Sólo números!!"}
    validates_format_of :email,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
    validates :direccion, :animal, :descripcion, :color, :peso, :tamano, :raza, :dueno, presence: true

def self.search(search)
  where("nombre LIKE ? OR edad LIKE ? OR raza LIKE ? OR dueno LIKE ? OR descripcion LIKE ?
  OR color LIKE ? OR peso LIKE ? OR tamano LIKE ? OR animal LIKE ?",
  "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%",
  "%#{search}%", "%#{search}%" , "%#{search}%") 
end

end
