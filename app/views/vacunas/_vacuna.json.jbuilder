json.extract! vacuna, :id, :nombre, :aplicacion, :doctor, :tipo, :created_at, :updated_at
json.url vacuna_url(vacuna, format: :json)
