# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170808010604) do

  create_table "companeros", force: :cascade do |t|
    t.string   "nombre"
    t.integer  "edad"
    t.string   "raza"
    t.string   "dueno"
    t.string   "descripcion"
    t.string   "color"
    t.float    "peso"
    t.string   "tamano"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "foto"
    t.string   "animal"
    t.float    "telefono"
    t.string   "direccion"
    t.string   "email"
    t.         "contacto"
  end

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.string   "nombre"
    t.date     "start"
    t.date     "end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "meetings", force: :cascade do |t|
    t.string   "name"
    t.datetime "start_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vacunas", force: :cascade do |t|
    t.string   "nombre"
    t.string   "aplicacion"
    t.string   "doctor"
    t.string   "tipo"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "companero_id"
    t.date     "fecha"
    t.index ["companero_id"], name: "index_vacunas_on_companero_id"
  end

end
