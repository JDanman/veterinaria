class AddAnimalToCompaneros < ActiveRecord::Migration[5.0]
  def change
    add_column :companeros, :animal, :string
  end
end
