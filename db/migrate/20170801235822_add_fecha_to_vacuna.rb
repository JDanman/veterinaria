class AddFechaToVacuna < ActiveRecord::Migration[5.0]
  def change
    add_column :vacunas, :fecha, :date
  end
end
