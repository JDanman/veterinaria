class AddContactoToCompanero < ActiveRecord::Migration[5.0]
  def change
    add_column :companeros, :contacto, :long
  end
end
