class CreateVacunas < ActiveRecord::Migration[5.0]
  def change
    create_table :vacunas do |t|
      t.string :nombre
      t.string :aplicacion
      t.string :doctor
      t.string :tipo

      t.timestamps
    end
  end
end
