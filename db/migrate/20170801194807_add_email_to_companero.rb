class AddEmailToCompanero < ActiveRecord::Migration[5.0]
  def change
    add_column :companeros, :email, :string
  end
end
