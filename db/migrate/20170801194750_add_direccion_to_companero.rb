class AddDireccionToCompanero < ActiveRecord::Migration[5.0]
  def change
    add_column :companeros, :direccion, :string
  end
end
