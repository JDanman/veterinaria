Rails.application.routes.draw do
  resources :meetings
  devise_for :users do
  get '/users/sign_out' => 'devise/sessions#destroy'
end
  resources :events
  resources :vacunas
  resources :companeros

  root to: 'companeros#index'

  
resources :companeros do
  resources :vacunas
end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
